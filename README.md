# File System

## Project setup
```
npm install
```

### Compiles and hot-reloads for development (Run Application on http://localhost:8080)
```
npm run serve
```

> ## Front-End Tree Map

* Src
	* **app.js (App Setting && Express Routes)**
    * **server.js (Port Listening)**
	* **db.js (DB && Table Creation)**
	* config
		* Images
    * routes
		* **index.js (Backend Router)**
	* controller
		* temp (File Operate Folder)
        * **file.js (File CRUD Methods)**
    * middleware
    * model
    * views
        * **index.ejs**
        * **new.ejs**
	    * **edit.ejs**
        * **main.js**
        * **style.css**
* **files.db (SQLite)**
* **out.txt (Temporary File Document)**


> ## View Page
> `src` ->  `views`  -> `index.ejs`
> ### ![](./img/ViewPage.png)
> ### File Locked
> ### ![](./img/FileLocked.png)
> ## New Page
> `src` ->  `views`  -> `new.ejs`
> ### ![](./img/NewPage.png)
> ## Edit Page
> `src` ->  `views`  -> `edit.ejs`
> ### ![](./img/EditPage.png)
	