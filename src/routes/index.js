const express = require('express');
const config = require('../config');
const file = require('../controller/file');

const router = express();

router.use('/file', file({ config }));

module.exports = router;