const app = require('./app');
const config = require('./config');

app.server.listen(config.port);

console.log(`Started on port ${app.server.address().port}`);
console.log(`Frontend: http://localhost:${app.server.address().port}`);
console.log(`API: http://localhost:${app.server.address().port}/v1`);