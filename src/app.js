// modules
const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
// files
const config = require("./config");
const routes = require("./routes")
const db =  require('./db');

// general setting 
let app = express();
app.server = http.createServer(app);
const defaultPath = '';

//CROS
app.use("*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS,PATCH");
  res.header("Cache-Control", "no-cache");
  // res.set('etag','strong');
  
  if (req.method === 'OPTIONS') {
    res.sendStatus(200)
  } else {
    next()
  }
});

app.use(bodyParser.urlencoded({extended: true}));

app.use(defaultPath + '/v1', routes);

// app.get(defaultPath + '/', (req,res)=> {
//     res.json({message: 'WEB BACKEND IS ALIVE!!'});
// });

app.get(defaultPath + '/v1', (req,res)=> {
    res.json({message: 'API V1 IS ALIVE!!'});
});

// front-end 
app.set('view engine', 'ejs');
function files() {
  return new Promise((resolve, reject) => {
      const data = [];
      db.each(`select * from files`, (err, row) => {
          if (err) {
              reject(err); // optional: you might choose to swallow errors.
          } else {
              data.push(row); // accumulate the data
          }
      }, (err, n) => {
          if (err) {
              reject(err); // optional: again, you might choose to swallow this error.
          } else {
              resolve(data); // resolve the promise
          }
      });
  });
}
app.get('/', async (req, res) => {
  try{
    let data = await files(); 
    console.log(data);
    res.render(__dirname + '/views/index.ejs',{files: data});
  }catch(e){
    console.log(e);
  }
});
app.get('/new', async (req, res) => {
  try{
    res.render(__dirname + '/views/new.ejs');
  }catch(e){
    console.log(e);
  }
});
app.get('/edit', async (req, res) => {
  try{
    let file = {
      id: req.query.id,
      name: req.query.name,
      content: req.query.content
    }
    console.log(file);
    let locked = false;
    db.get(`select * from files where id=${req.query.id}`,(err,row) => {
        if(err) console.log(err);
        else{
          file.name = row.name;
          file.content = row.text;
          locked = row.locked;
          if(locked) res.redirect('/');
          else{
            db.run(`update files set locked=${true} where id=${req.query.id}`);
            setTimeout(function() {
              db.run(`update files set locked=${false} where id=${req.query.id}`);
            }, 60 * 1000);
            res.render(__dirname + '/views/edit.ejs', {file: file});
          }
        }
    });
  }catch(e){
    console.log(e);
  }
});

module.exports = app;