const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('files.db');

db.run(`CREATE TABLE IF NOT EXISTS files (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, text BLOB, locked boolean DEFAULT false);
`);

db.all("select name from sqlite_master where type='table'", function (err, tables) {
    console.log(tables);
});

module.exports = db;