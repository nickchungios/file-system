const  { Router } = require('express');
const db =  require('../db');
const fs = require('fs');
const fsAsync = require('fs').promises;
const multer = require('multer');

let upload = multer({dest: __dirname+'/temp'});
let type = upload.single('file');

module.exports = ((config)=> {
    let api = Router();
    api.get('/', (req,res) => {
        db.each(`select * from files`, (err,row) => {
            if(err) console.log(err);
            else console.log(row);
        });
    });
    api.get('/delete',(req,res) => {
        db.run(`delete from files where id=${req.query.id}`);
        res.redirect('/');
    });
    api.get('/download', (req,res) => {
        let file,path;
        db.get(`select * from files where id=${req.query.id}`,(err,row) => {
            if(err) console.log(err);
            else{
                file = row.text;
                path = __dirname + `/temp/${row.name}`;
                fs.writeFileSync(path,file,'utf8');
                res.attachment(path);
                let readStream = fs.createReadStream(path);
                readStream.setEncoding('utf8');
                readStream.pipe(res).once("close", function () {
                    readStream.destroy();
                    fs.unlinkSync(path);
                });
            }
        });
    });
    api.post('/add',(req,res) => {
        console.log(req.body);
        if(!req.body) res.status(404).send();
        fs.writeFileSync('out.txt',req.body.content,'utf8');
        let file = fs.readFileSync('out.txt','utf8');
        db.run(`INSERT INTO files (name, text) VALUES ( '${req.body.fileName}.txt', "${file}");`);
        res.redirect('/new');
    });
    api.post('/update',(req,res) => {
        console.log(req.body);
        if(!req.body) res.status(404).send();
        let id = Number(req.body.id);
        fs.writeFileSync('out.txt',req.body.content,'utf8');
        let file = fs.readFileSync('out.txt','utf8');
        db.run(`update files set name="${req.body.fileName}",text="${file}" where id=${id}`);
        db.run(`update files set locked=${false} where id=${id}`);
        res.redirect('/');
    });
    return api;
});